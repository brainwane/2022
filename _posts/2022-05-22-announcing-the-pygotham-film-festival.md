---
layout: post
title: Announcing The PyGotham Film Festival
date: 2022-05-22 17:17 -0400
---
PyGotham is back for 2022 with our next iteration of the PyGotham TV conference
format.

## What is The PyGotham Film Festival?

PyGotham is a regional Python conference that has been serving the Python
community in New York City and beyond since 2011. In 2020 and 2021, the
conference became PyGotham TV, an online-only event that encouraged presenters
to re-imagine their conference talks as television shows or broadcasts. In
keeping with our tradition of being quite different, The PyGotham Film Festival
takes that theme a step further.

We hope to make The PyGotham Film Festival the first conference of its kind
(that we know of). We’re asking presenters to pitch short films instead of
standard tech talks. PyGotham has always asked for talks covering “anything that
would be of interest to the Python community”, and this year is no different. So
long as your film’s topic is relevant to members of the Python community, it’s
up for proposal and consideration. PyGotham typically includes presentations on
topics such as web and systems development, machine learning and data science,
tech ethics, and community building. These categories of topics (and more) are
all still welcome at this year’s festival, and we invite presenters to view them
through a different lens than they normally might. See the list below for
examples of the kinds of talks we hope to accept:

- [A Monte Carlo Murder Mystery: A Holmes-Watson-PyMC3
  adventure!](https://2020.pygotham.tv/talks/a-monte-carlo-murder-mystery-a-holmes-watson-pymc3-adventure/)
- [Former Hacker Reviews Iconic Python/Rust Hacking Scenes From Movies &
  TV](https://2020.pygotham.tv/talks/former-hacker-reviews-iconic-python-rust-hacking-scenes-from-movies-tv/)
- [I am the very model of a modern Django
  ModelForm](https://www.youtube.com/watch?v=D1FpYhDDjvQ)
- [The Detective Has Arrived](https://2020.pygotham.tv/talks/the-detective-has-arrived/)

## What are the requirements?

- All films must abide by our [Code of Conduct]({% link about/code-of-conduct.md
  %}).
- Films must be of interest to the Python community. These interests are vast
  and varied, so don’t let this discourage you! Have an idea and want to discuss
  it with the organizers? You can reach us at
  [program@pygotham.org](mailto:program@pygotham.org). As general guidelines:
    - A film about the impacts of technology and the kind of work that the
      Python community may participate in is a good fit. A romantic comedy or
      horror film without Pythonic or other technological themes may not be.
    - A dramatic depiction of a Python tool or feature is a good fit. One that
      is specific to another programming language not applicable to Python or
      technology in general may not be.
    - A parody of an existing type of film or television show that ties into
      Python-related topics is a good fit. A shot-for-shot remake of an existing
      Monty Python skit is not.
- Films must be between five and twenty-five minutes long.
- Content should be suitable for all ages. In addition to our code of conduct
  requirement, films should be family and workplace friendly, as PyGotham’s
  audience spans all ages.
- Specific file format and other technical requirements will follow.


## How can the organizers help me with my film?

We’re glad you asked! We want to help everyone make the best film possible. Here
are a couple of ways in which we can do that:

- Organizers will be available to discuss your proposal with you to make sure
  it’s a good fit for the festival and answer any questions you have regarding
  how or if a topic is suitable.
- The festival can help with modest funding requirements upon request. If your
  film is within reach but lacking a specific piece of gear, we’ll do our best
  to work with you to make it happen.


## I have another question!

Feel free to contact the team at
[organizers@pygotham.org](mailto:organizers@pygotham.org).
